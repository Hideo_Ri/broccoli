package com.hideo.broccoli.demo.helloworld.controller;

import com.hideo.broccoli.demo.helloworld.data.Greeting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping(path = "/broccoli/demo/helloworld")
public class HelloWorldController {
    private static final Logger logger = LoggerFactory.getLogger(HelloWorldController.class);

    private final AtomicLong counter = new AtomicLong();

    @GetMapping(path = "/hello")
    public Greeting hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        logger.info("Start hello()...");
        Greeting ret = new Greeting(counter.incrementAndGet(), String.format("Hello %s!", name));
        logger.info("End hello()...");
        return ret;
    }
}
